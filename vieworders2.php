<?php

//cria nome de uma variável abreviado
  $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

?>

<html>
<head>
  <title>Bob's Auto Parts - Customer Orders</title>
</head>
<body>
<h1>Bob's Auto Parts</h1>
<h2>Customer Orders</h2>

<?php

// Lê o arquivo inteiro
// Cada pedido torna-se um elemento no array
$orders = file("$DOCUMENT_ROOT/orders/orders.txt");

// Conta o número de pedidos no array
$number_of_orders = count($orders);
if ($number_of_orders < 1)
{
  echo '<p><strong>No orders peding.
        Please try again later.</strong></p>';
}

echo "<table border=1>\n";
echo '<tr><th bgcolor="CCCCFF">Order Date</th>
          <th bgcolor="CCCCFF">Tires</th>
          <th bgcolor="CCCCFF">Oil</th>
          <th bgcolor="CCCCFF">Spark</th>
          <th bgcolor="CCCCFF">Total</th>
          <th bgcolor="CCCCFF">Address</th>
      </tr>';

for ($i = 0; $i < $number_of_orders; $i++) 
{
  // Divide cada linha
  $line = explode(. , $orders[$i] );

  // Mantém somente o número de itens encomendados
  $line[1] = intval( $line[1] );
  $line[2] = intval( $line[2] );
  $line[3] = intval( $line[3] );
  // Envia cada pedido para a saída

  echo "<tr><td>$line[0]</td>
            <td align='right'>$line[1]</td>
            <td align='right'>$line[2]</td>
            <td align='right'>$line[3]</td>
            <td align='right'>$line[4]</td>
            <td>$line[5]</td>";
  }

  echo "</table>";
?>
</body>
</html>
